import sys

def three_fib(n):
    if n < 3:
        return n
    else:
        return 3 * three_fib(n - 3) + 2 * three_fib(n - 2) + three_fib(n - 1)

if __name__ == '__main__':
    inp = input("Type random stuff:")
    sys.stdout.flush()
    print(three_fib(6))
    print(three_fib(2))
