tempStr = input("请输入温度：")
if tempStr[-1] in ['F', 'f']:
    print("转换后的温度：" + str(float(tempStr[:-1] - 32)/1.8))
elif tempStr[-1] in ['C', 'c']:
    print("转换后的温度：" + str(float(tempStr[:-1]) * 1.8 + 32))
else:
    print("Error")