#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>

auto fib(int n) -> int {
    std::vector<int> v(n);
    return std::accumulate(v.cbegin(), v.cend(), std::pair{1, 0},
        [](auto pair, auto _) {
            auto [a, b] = pair;
            return std::pair{b, a+b};
        }).second;
}

int main() {
    int result = fib(6);
    std::cout << result << std::endl;
}