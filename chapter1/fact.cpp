#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

auto fact(int n) -> int {
    std::vector<int> v(n);
    std::iota(v.begin(), v.end(), 1);
    return std::accumulate(v.cbegin(), v.cend(), 1,
        [](auto acc, auto _){return acc * _;});
}

int main() {
    int result = fact(5);
    std::cout << result <<std::endl;
}
