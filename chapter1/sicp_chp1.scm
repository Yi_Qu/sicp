#lang racket

(define (abs x)
    (if (< x 0) (- x) x))
(abs -4)
(abs 5)