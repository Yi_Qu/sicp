#lang racket


(define (square x) (* x x))

(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))
(fast-expt 3 5)


(define (fast-expt-iter b n)
  (fast-expt-iter-imp 1 b n))

(define (fast-expt-iter-imp a b n)
  (cond ((= n 1) (* a b))
        ((even? n) (fast-expt-iter-imp a (square b) (/ n 2)))
        (else (fast-expt-iter-imp (* a b) b (- n 1)))))
(fast-expt-iter 3 5)


(define (double n) (* n 2))

(define (halve n) (floor (/ n 2)))

(define (multiply a b)
  (cond ((= b 0) 0)
        ((even? b) (multiply (double a) (halve b)))
        (else (+ a (multiply a (- b 1))))))
(multiply 3 5)

(define (multiply-iter a b)
  (multiply-iter-imp 0 a b))

(define (multiply-iter-imp acc a b)
  (if (= b 0)
    acc
    (cond ((even? b) (multiply-iter-imp acc (double a) (halve b)))
          (else (multiply-iter-imp (+ acc a) a (- b 1))))))
(multiply-iter 3 5)
