#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>

auto fact_acc(int n) -> int {
    std::vector<int> v(n);
    for(auto i: v)
        std::cout << i << ' ';
    std::cout << '\n';
    std::iota(v.begin(), v.end(), 1);
    for(auto i: v)
        std::cout<< i << ' ';
    std::cout << '\n';
    return std::accumulate(v.cbegin(), v.cend(), 1,
        [](auto acc, auto left){
            std::cout << acc << std::endl;
            std::cout << left << std::endl;
            return acc * left;
        });
}

int main() {
    int result = fact_acc(5);
    std::cout << result << std::endl;
}
