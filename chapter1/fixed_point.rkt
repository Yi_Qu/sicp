#lang racket

(define tolerance 0.00001)

(define (fixed_point func init-val)
	(define (close-enouph? a b)
		(< (abs (- a b)) tolerance))
	(define (try guess)
		(let ((next (func guess)))
			(if (close-enouph? guess next)
				next
				(try next))))
	(try init-val))
