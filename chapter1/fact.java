import java.util.stream.IntStream;


public class fact {
    public static int get_fact(int val) {
        return IntStream.rangeClosed(1, val).reduce(1, (acc, n) -> acc * n);
    }

    public static void main(String[] args) {
        int f = get_fact(5);
        System.out.println(f);
    }
}
